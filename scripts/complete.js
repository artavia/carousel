var DRYOBJ = ( function ( ) {
	
	/*################  ES5 pragma  ######################*/
	'use strict';	

	var _arVendorPREs = [ "moz", "ms", "webkit", "o" ];
	var _bpms = 15.46875;
	var _window = window; 

	function _AddEventoHandler( nodeFlanders, type, callback ) {
		if( type !== "DOMContentLoaded") { 
			if( nodeFlanders.addEventListener ) { 
				nodeFlanders.addEventListener( type, callback, false);
			}		
			else	
			if( nodeFlanders.attachEvent ) { 
				nodeFlanders.attachEvent( "on" + type, callback );
			} 		
			else { 
				nodeFlanders["on" + type] = callback; 
			}
		}
		else 
		if( type === "DOMContentLoaded" ) { 
			if( nodeFlanders.addEventListener ) { 
				nodeFlanders.addEventListener( type, callback, false);
			}
			else 
			if( nodeFlanders.attachEvent ) { 
				if( nodeFlanders.readyState === "loading" ) {
					nodeFlanders.onreadystatechange = callback;
				}
			}
			else { 
				nodeFlanders["on" + type] = callback; 
			}
		}
	}

	function _RetEVTsrcEL_evtTarget( leEvt ) { 
		if( typeof leEvt !== "undefined") { 
			var _EREF = leEvt; 
		}
		else {
			var _EREF = window.event; 
		}
		if( typeof _EREF.target !== "undefined") {
			var evtTrgt = _EREF.target;	
		}
		else {
			var evtTrgt = _EREF.srcElement; 
		}
		return evtTrgt;
	}
	
	function _RetPropertyRoutine( pm1, pm2 ) { 
		
		var ar_vendorPreez = _arVendorPREs; 
		
		var clCharMax = ar_vendorPreez.length; 
		var leProp;
		var dL;
		var param = pm1; // getUserMedia
		var paramEl = pm2; // Navigator
		var len = param.length; 
		var nc = param.slice( 0,1 ); // g
		var Uc = param.slice( 0,1 ).toUpperCase(); // G
		var Param = param.replace( nc, Uc ); // GetUserMedia	
		if ( param in paramEl ) { 
			leProp = param; 
		} 
		for ( dL = 0; dL < clCharMax; dL = dL + 1) { 
			if ( ar_vendorPreez[ dL ] + Param in paramEl ) { 
				leProp = ar_vendorPreez[ dL ] + Param; 
			} 
		} 
		return leProp;
	}

	// END of _private properties 
	return {
		rafUtils : { 
			get_bpms: function() {
				return _bpms; 
			} 
		} , 
		RetELs : {
			glow : function() { 
				return _window; 
			}  
		} ,
		Utils : {
			evt_u : {
				AddEventoHandler : function( nodeFlanders, type, callback ) {
					return _AddEventoHandler( nodeFlanders, type, callback );
				} , 
				RetEVTsrcEL_evtTarget : function( leEvt ) {
					return _RetEVTsrcEL_evtTarget( leEvt );
				} 
			}
		} , 
		ReturnProps : { 
			RetPropertyRoutine : function( pm1, pm2 ) {
				return _RetPropertyRoutine( pm1, pm2 );
			} 
		} // END DRYOBJ.ReturnProps 

	}; // END public properties
	
}( ) ); // console.log( DRYOBJ ); 


var Gh_pages = ( function() {	
	
	/*################  ES5 pragma  ######################*/
	'use strict';		
	
	var _docObj = window.document;
	
	var _mrBeepersMS;
	var _RAF;
	var _CAF;	
	
	var _singleImgWidth; 
	var _totalWidthImgs; 
	var _arPush_ImgWidths = []; 
	var _arPush_ImgELs = []; 
	var _ar_ImgCt; 
	
	var _endVal; // 500px  
	var _startVal; // 0 -500 -1000 -1500 	
	
	var _pmAvgMultiple; 
	var _pmEndV; 	
	
	var _loopTimer_caro; 
	var _curImg; 	
		
	var _el_AvgBtnCon = _docObj.getElementById( "AvgBtnCon" ); 
	var _el_AvgWideCan = _docObj.getElementById( "AvgWideCan" ); 
	var _el_AverageIn = _docObj.getElementById( "AverageIn" ); 
	var _el_AverageOut = _docObj.getElementById( "AverageOut" ); 	
	
	var _all_imgs = _docObj.getElementsByTagName( "img" ); 
	var _I; 	
	
	var _all_btns = _docObj.getElementsByTagName( "button" ); 
	var _btnCt = _all_btns.length; 
	var _B; 

	var _RunCarousel = function( leEvt ) {
	
		var evtTrgt = DRYOBJ.Utils.evt_u.RetEVTsrcEL_evtTarget( leEvt ); 

		if( _docObj.documentElement.addEventListener ){ 
			var dataAttrib = evtTrgt.dataset.dir; 
		}
		if( _docObj.documentElement.attachEvent ){
			var dataAttrib = evtTrgt.getAttribute( "data-dir" );
		}
		if( dataAttrib === "prev" ){
			if( _curImg <= 1 ) {
			
				_curImg = _ar_ImgCt;
				_endVal = _singleImgWidth * (_curImg - 1);
				
				dataAttrib === "next";
			}
			else {
				_curImg = _curImg - 1;
				_endVal = _singleImgWidth * (_curImg - 1);
			}
		}
		else
		if( dataAttrib === "next" ){ 
			if( _curImg >= _ar_ImgCt ) {
			
				_curImg = 1;
				_endVal = _singleImgWidth * (_curImg - 1);
				
				dataAttrib === "prev";
			} 
			else {
				_curImg = _curImg + 1;
				_endVal = _singleImgWidth * (_curImg - 1);
			}
		}		
		if( _endVal !== 0 ) {
			_endVal = -_endVal;
		}
		else 
		if( _endVal === 0 ) {
			_endVal = _endVal;
		} 
		_Anim_marginLeft( _el_AvgWideCan , _endVal ); 		
	};
	
	function _Anim_marginLeft( _pmAvgMultiple, _pmEndV ) {
		_mrBeepersMS = DRYOBJ.rafUtils.get_bpms(); 
		_RAF = DRYOBJ.ReturnProps.RetPropertyRoutine( "requestAnimationFrame" , DRYOBJ.RetELs.glow() ); 
		_CAF = DRYOBJ.ReturnProps.RetPropertyRoutine( "CancelAnimationFrame" , DRYOBJ.RetELs.glow() ); 
		
		_pmAvgMultiple = _el_AvgWideCan; 
		_pmEndV = _endVal; 
		
		if( parseInt( _pmAvgMultiple.style.marginLeft, 10 ) > _pmEndV ){
			
			if( parseInt( _pmAvgMultiple.style.marginLeft, 10 ) !== _pmEndV ) { 
			
				_pmAvgMultiple.style.marginLeft = parseInt( _pmAvgMultiple.style.marginLeft, 10 ) - 50 + "px"; 
				
				if( window.requestAnimationFrame || window[ _RAF ] ) { 
					_loopTimer_caro = (function( self ) {
						return window[ _RAF ]( _Anim_marginLeft ); 
					})( _loopTimer_caro ); 
				}
				
				else { 
					_loopTimer_caro = (function( self ) {
						return setTimeout( _Anim_marginLeft , _mrBeepersMS ); 
					})( _loopTimer_caro ); 
				}
			}
		}
		
		else
		if( parseInt( _pmAvgMultiple.style.marginLeft, 10 ) < _pmEndV ){
			
			if( parseInt( _pmAvgMultiple.style.marginLeft, 10 ) !== _pmEndV ) { 
				
				_pmAvgMultiple.style.marginLeft = parseInt( _pmAvgMultiple.style.marginLeft, 10 ) + 50 + "px"; 
				
				if( window.requestAnimationFrame || window[ _RAF ] ) { 
					_loopTimer_caro = (function( self ) {
						return window[ _RAF ]( _Anim_marginLeft ); 
					})( _loopTimer_caro ); 
				}
				
				else { 
					_loopTimer_caro = (function( self ) {
						return setTimeout( _Anim_marginLeft , _mrBeepersMS ); 
					})( _loopTimer_caro ); 
				}
			}
		}
		
		else 
		if( parseInt( _pmAvgMultiple.style.marginLeft, 10 ) === _pmEndV ) { 

			if( window.cancelAnimationFrame || window[ _CAF ] ) { 
				_loopTimer_caro = ( function( self ) {
					return window[ _CAF ]( _loopTimer_caro );
				})( _loopTimer_caro ); 
			}
			else { 
				_loopTimer_caro = ( function( self ) {
					return clearTimeout( _loopTimer_caro ); 
				})( _loopTimer_caro );
			}
			
			_pmAvgMultiple.style.marginLeft = parseInt( _pmAvgMultiple.style.marginLeft, 10 ) + "px"; 
		}
	} 
	
	function _LOAD() {
		for ( _I = 0; _I < _all_imgs.length; _I = _I + 1 ) {
			if ( _all_imgs[ _I ].className === "caroImg" ) { 
				_arPush_ImgELs.push( _all_imgs[ _I ] );
				_arPush_ImgWidths.push( _all_imgs[ _I ].width );
			}
		}		
		
		if(document.documentElement.addEventListener){
			_ar_ImgCt = _arPush_ImgELs.length;
			_singleImgWidth = _arPush_ImgWidths[ 0 ];
			_totalWidthImgs = _ar_ImgCt * _singleImgWidth;
			
			_el_AverageOut.style.width = _singleImgWidth + "px"; 
			_el_AverageIn.style.width = _singleImgWidth + "px";	
		}
		else
		if(document.documentElement.attachEvent){
			_el_AverageOut.style.width = "500px";
			_el_AverageIn.style.width = "500px";
		}
		
		_ar_ImgCt = _arPush_ImgELs.length; 
		_singleImgWidth = _arPush_ImgWidths[ 0 ]; 
		_totalWidthImgs = _ar_ImgCt * _singleImgWidth; 
		
		_el_AvgBtnCon.style.display = "block";
		_el_AvgWideCan.style.width = _totalWidthImgs + "px"; 
		
		if( !_startVal ){
			_startVal = 0; 
			_el_AvgWideCan.style.marginLeft = _startVal + "px"; 
		}
		else 
		if( !!_startVal ){
			_startVal = parseInt( _el_AvgWideCan.style.marginLeft, 10 ); 
			_el_AvgWideCan.style.marginLeft = _startVal + "px"; 
		}
		for( _B = 0; _B < _btnCt; _B = _B + 1 ){
			if( _all_btns[ _B ].className === "caroButton" ) {
				DRYOBJ.Utils.evt_u.AddEventoHandler( _all_btns[ _B ], "click", _RunCarousel );	
			}
		} 
		
	}

	// END of _private properties
	return {
		InitLoad: function() {

			_curImg = 1; 
			_startVal = null;
			_loopTimer_caro = null;

			return _LOAD(); 
		} // window.Gh_pages.InitLoad()
	};
} )(); // window.Gh_pages

DRYOBJ.Utils.evt_u.AddEventoHandler( window, "load" , Gh_pages.InitLoad() ); 