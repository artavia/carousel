# Brief description
An IE&#45;friendly carousel&#45;&#45; for edge&#45;cases requiring versions 8 or less specifically &#45;&#45;which demonstrates feasibility in terms of javascript mechanics.

## Please visit
You can see [the project](https://artavia.gitlab.io/carousel "the project at github") for yourself and decide whether it is really for you or not. 

## Live Implementation
I used to have a similar copy but it carried a hack. In fact, the hack consisted of a transparent gif to accompany the image&#45;container elsewhere. Then, with javascript I could recall the width of that transparent gif image and consequently base the width of the image&#45;container. It was difficult, imperfect, and lightweight and perhaps a little fragile. In the near future, I will have a more current methodology in place sans the accompanying transparent gif image. Resizing images is costly enough as it is.

